package classes_task.data;


public class Customer {
    private final int id;
    private final String surname;
    private final String name;
    private final String patronymic;
    private final int creditCardNumber;
    private final int bankAccountNumber;

    public Customer(int id,String surname, String name, String patronymic, int creditCardNumber, int bankAccountNumber){
        this.id = id;
        this.surname = surname;
        this.name = name;
        this.patronymic = patronymic;
        this.creditCardNumber = creditCardNumber;
        this.bankAccountNumber = bankAccountNumber;
    }

    public int getId() {
        return id;
    }

    public String getSurname() {
        return surname;
    }

    public String getName() {
        return name;
    }

    public String getPatronymic() {
        return patronymic;
    }

    public int getCreditCardNumber() {
        return creditCardNumber;
    }

    public int getBankAccountNumber() {
        return bankAccountNumber;
    }
}
