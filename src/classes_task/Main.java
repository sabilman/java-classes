package classes_task;

import classes_task.data.Customer;

import java.util.ArrayList;

public class Main {
    public static void main(String[] args) {
        ArrayList<Customer> customers = new ArrayList<Customer>();
        ArrayList<Customer> newCustomers = new ArrayList<Customer>();

        customers.add(new Customer(00100,"Usserova","Sabillya","Duisenovna", 1000,0001000));
        customers.add(new Customer(00200,"Aitbayeva","Malika","Bayurzhanovna", 2000,0002000));
        customers.add(new Customer(00400,"Sapabek","Danil","Kuanyshevich", 4000,0004000));

       // Sort customers in alphabetic order
       customers.sort((a,b) -> a.getSurname().compareTo(b.getSurname()));

       // get customers with credit card number in interval
       for(int i=0; i<customers.size();i++){
           System.out.println(customers.get(i).getSurname());
           if(customers.get(i).getCreditCardNumber()<2000){
               newCustomers.add(customers.get(i));
           }
       }
    }
}

